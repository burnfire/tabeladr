#pragma once
#include "RepoAbstract.h"
#include "Subiect.h"

/*Substanta, cantitate per pachet, cantitate totala*/
typedef tuple<Substanta, double, double> DTOScor;

class ServiceSubstante : public Subiect
{
private:
	RepoSubstanta& repo;
public:
	/*ctor*/
	ServiceSubstante(RepoSubstanta& repo) :repo{ repo } {}
	/*add - adauga o substanta
	in: Cod ONU:string, denumire:string, clasa:string, clasificare: string, cantitLimitata:double, grTransp:int, situatie:string (default="OK")
	out: -
	raises: DomainError, daca datele nu sunt valide
			RepoError, daca codul ONU exista deja*/
	void add(string cod, string denumire, string clasa, string clasif, double cantitLimit, int grTransp,string situatie="OK");
	/*modify - modifica o substanta
	in: Codul ONU al substantei de modificat: string, denumire noua:string, clasa noua: string, clasificare noua: string, cantitLimitate: double, grTransp:int, situatie:string (default="OK")
	raises: DomainError, daca noile date nu sunt valide
			RepoError, daca codul ONU nu exista*/
	void modify(string cod, string denumire, string clasa, string clasif, double cantitLimit, int grTransp,string situatie="OK");
	/*remove - sterge o substanta
	in: Cod ONU:string
	out: -
	raises: RepoError, daca codul ONU nu exista*/
	void remove(string cod);
	/*getAll - obtine toate substantele din aplicatie
	in: -
	out: rez:vector<Substanta>*/
	vector<Substanta> getAll() const;
	/*cautaCod - cauta o substanta dupa codul ONU
	in: cod:string
	out: rez:Substanta
	raises: RepoError, daca codul ONU nu exista*/
	Substanta cautaCod(string cod) const;
	/*sort - sorteaza dupa un criteriu
	in: criteriu: bool (*f)(Substanta a, Substanta b), reverse:bool
	out: vector<Substanta>*/
	vector<Substanta> sort(bool (*f)(Substanta a, Substanta b),bool reverse=false);
	/*getAllCodes - obtine toate codurile din aplicatie
	in: -
	out: coduri:vector<string>*/
	vector<string> getAllCodes() const;
};

class ServiceTransportabilitate
{
private:
	vector<DTOScor> all;
	double scor;
	double greutate;
	int situatie = -1;//-1 - necalculat, 0 - fara placute, 1 - placuta alb/negru, 2 - placuta portocalie, 3 - avem una sau mai multe substante interzise
	set<int> indecsiInterzise;//indexul substantelor care sunt interzise transportului
	set<int> indecsiNonADR;//indexul substantelor introduse care nu fac parte din ADR
public:
	/*Service care foloseste DTO tuple<Substanta,double,int> (DTOScor) pentru calcularea scorurilor necesare*/
	/*add - adauga un DTO in lista
	in: s:Substanta, cantitPachet:double, cantitTotala:double
	out: -*/
	void add(const Substanta& s,double cp,double ct);
	/*clear - sterge toate obiectele din lista
	in: -
	out: -*/
	void clear();
	/*getGreutateCantitLimitate - returneaza greutatea totala din cantitatile limitate, adica suma cantitatilor totale ale substantelor, calculata anterior
	in: -
	out: suma:double*/
	double getGreutateCantitLimitate() const;
	/*getScor - returneaza scorul pentru substantele care nu se incadreaza la cantitati exceptate
	in: -
	out: scor:double*/
	double getScor() const;
	/*getSituatie - returneaza situatia calculata
	in: -
	out: situatie:int, unde 0 - fara placute, 1 - placuta alb/negru, 2 - placuta portocalie*/
	int getSituatie() const;
	/*calculeaza - realizeaza calculele de scor si greutate si determina daca exista substante interzise. Daca exista, adauga indexul lor in indecsiInterzise
	in: -
	out: -*/
	void calculeaza();
	/*calculeazaIndex - calculeaza scorul si greutatea pentru un singur item
	in: index:int
	out: rez:pair<double,double>, (greutate,scor), sau (0,0) daca substanta de la index nu face parte din ADR*/
	pair<double, double> calculeazaIndex(int index) const;
	/*pop - sterge ultimul element din lista
	in: -
	out: -*/
	void pop();
	/*getIndecsiSubstanteInterzise - obtine indecsii cu substantele interzise obtinute in urma calculului
	in: -
	out: rez:set<int>&*/
	const set<int>& getIndecsiSubstanteInterzise() const noexcept;
	/*getIndecsiSubstanteNonADR - obtine multimea indecsilor substantelor din tabel care nu fac parte din ADR
	in: -
	out: rez:set<int>&*/
	const set<int>& getIndecsiSubstanteNonADR() const noexcept;
};