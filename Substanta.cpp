#include "Substanta.h"
#include "ValidatorSubstanta.h"

bool Substanta::validare() const
{
	if (ValidatorSubstanta::testCampuriGoale(*this))//Sunt campurile goale?
		return false;
	if (!ValidatorSubstanta::testClasaCorecta(*this))//Clasa introdusa este corecta?
		return false;
	if (!ValidatorSubstanta::testGrupaTransportCorecta(*this))//Grupa de transport este incorecta
		return false;
	if (!ValidatorSubstanta::testSituatieCorecta(*this))//Este situatia corecta?
		return false;
	if (!ValidatorSubstanta::testCantitateCorecta(*this))//Este cantitatea limitata corecta?
		return false;
	return true;
}

const string& Substanta::getCod() const
{
	return cod;
}

const string& Substanta::getDenumire() const
{
	return denumire;
}

const string& Substanta::getClasa() const
{
	return clasa;
}

const string& Substanta::getClasificare() const
{
	return clasif;
}

const double& Substanta::getCantitLimitata() const
{
	return cantitLimit;
}

const int& Substanta::getGrupaTransport() const
{
	return grTransp;
}

const bool Substanta::esteInterzis() const
{
	return situatie=="INTERZIS";
}

const bool Substanta::esteNonADR() const
{
	return situatie=="nonADR";
}

string Substanta::serialize() const
{
	return getCod()+";"+getDenumire()+";"+getClasa()+";"+getClasificare()+";"+to_string(getCantitLimitata())+";"+to_string(getGrupaTransport())+";"+situatie;
}

bool Substanta::critCod(Substanta a, Substanta b)
{
	return a.getCod()<b.getCod();
}

bool Substanta::critDenumire(Substanta a, Substanta b)
{
	return a.getDenumire()<b.getDenumire();
}

Substanta DeserializatorSubstanta::deserialize(const std::string sir)
{
	vector<string> parti;
	stringstream sep(sir);
	string s;
	while (getline(sep, s, ';'))
	{
		parti.push_back(s);
	}
	parti[0].erase(remove(parti[0].begin(), parti[0].end(), '\r\n'), parti[0].end());
	/*Daca nu avem completate campurile, de exemplu avem o substanta interzisa, punem 0*/
	if (parti[4].empty())
		parti[4] = "0";
	if (parti[5].empty())
		parti[5] = "0";
	return Substanta(parti[0], parti[1], parti[2], parti[3],stod(parti[4]),stoi(parti[5]),parti[6]);
}
