#pragma once
#include "Substanta.h"
class ValidatorSubstanta
{
public:
	/*testCampuriGoale - verifica daca substanta nu are campuri goale
	in: s:Substanta
	out: true/false, daca sunt sau nu campuri goale*/
	static bool testCampuriGoale(Substanta s);
	/*testClasaCorecta - verifica daca clasa substantei este corecta
	in: s:Substanta
	out: true/false, daca clasa este sau nu corecta*/
	static bool testClasaCorecta(Substanta s);
	/*testGrupaTransportCorecta - verifica daca grupa de transport a substantei este corecta (0,1,2,3 sau 4)
	in: s:Substanta
	out: true/false, daca grupa este corecta sau nu*/
	static bool testGrupaTransportCorecta(Substanta s);
	/*testSituatieCorecta - verifica daca situatia substantei este corecta (OK, INTERZIS sau nonADR)
	in: s:Substanta
	out: true/false, daca situatia este corecta sau nu*/
	static bool testSituatieCorecta(Substanta s);
	/*testCantitateCorecta - verifica daca cantitatea limitata a substantei este corecta (pozitiva)
	in: -
	out: true/false, daca cantitatea respecta regula sau nu*/
	static bool testCantitateCorecta(Substanta s);
};

