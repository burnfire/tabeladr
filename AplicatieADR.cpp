﻿#include "AplicatieADR.h"

AplicatieADR::AplicatieADR(ServiceSubstante& s, QWidget *parent)
    : serviceSubstanta{ s },QWidget(parent)
{
    init();
    connect();
}

void AplicatieADR::init()
{
    setLayout(layMain);
    layMain->addWidget(textSus);
    layMain->addLayout(layBtns);
    layMain->addWidget(boxLegend);

    layBtns->addWidget(btnView, 1, 1);
    layBtns->addWidget(btnCRUD, 1, 2);
    layBtns->addWidget(btnScore, 2, 1);

    boxLegend->setTitle("Legenda");
    boxLegend->setLayout(layBoxLegend);

    layBoxLegend->addWidget(txtCulInterzis);
    layBoxLegend->addWidget(txtCulOK);
    layBoxLegend->addWidget(txtCulScor);
    layBoxLegend->addWidget(txtCulCantitLimit);
    
    txtCulInterzis->setStyleSheet("QLabel { background-color : red; }");
    txtCulOK->setStyleSheet("QLabel { background-color : green; }");
    txtCulScor->setStyleSheet("QLabel { background-color : orange; }");
    txtCulCantitLimit->setStyleSheet("QLabel { background-color : black; color : white }");
}

void AplicatieADR::connect()
{
    QPushButton::connect(btnView, &QPushButton::clicked, [&]() {showViewer(); });
    QPushButton::connect(btnCRUD, &QPushButton::clicked, [&]() {showCRUD(); });
    QPushButton::connect(btnScore, &QPushButton::clicked, [&]() {showScoreWindow(); });
}

void AplicatieADR::showViewer() const
{
    ViewerWindow* tmp = new ViewerWindow(serviceSubstanta);
    tmp->show();
}

void AplicatieADR::showCRUD() const
{
    CRUDWindow* tmp = new CRUDWindow(serviceSubstanta);
    tmp->show();
}

void AplicatieADR::showScoreWindow() const
{
    ScoreWindow* tmp = new ScoreWindow(serviceSubstanta);
    tmp->show();
}

void AplicatieADR::verificaCampuriInput() const
{
}

void ViewerWindow::updateTable()
{
    auto all = s.getAll();
    tabelSubstante->setRowCount(0);
    for (const auto& x : all)
    {
        auto index = tabelSubstante->rowCount();
        tabelSubstante->insertRow(index);
        completeazaRand(index, x);
        formateazaRand(index, x);
    }
    textNr->setText("Sunt " + QString::number(all.size()) + " substante.");
}

void ViewerWindow::updateSelf()
{
    updateTable();
}

void ViewerWindow::cautaDupaCod()
{
    try
    {
        verificaCampuriInput();
        auto subst = s.cautaCod(codIn->text().toStdString());
        tabelSubstante->setRowCount(1);
        completeazaRand(0, subst);
        for(int i=0;i<=5;i++)
            tabelSubstante->item(0, i)->setBackgroundColor(QColor::fromRgb(0, 255, 0));
        formateazaRand(0, subst);
    }
    catch (const Eroare& ex)
    {
        msjEroare(ex.getError());
    }
}

void ViewerWindow::formateazaRand(int index, const Substanta& s)
{
    if (s.esteInterzis())
    {
        for (int i = 0; i < tabelSubstante->columnCount(); i++)
        {
            tabelSubstante->item(index, i)->setBackgroundColor(culoareInterzis);
        }
    }
    else if (s.esteNonADR())
    {
        for (int i = 0; i < tabelSubstante->columnCount(); i++)
        {
            tabelSubstante->item(index, i)->setBackgroundColor(culoareGasit);
        }
    }
}



void ViewerWindow::verificaCampuriInput() const
{
    if (codIn->text().isEmpty())
        throw UIError("Cod ONU vid!");
}

void ViewerWindow::completeazaRand(int index,const Substanta& s)
{
    tabelSubstante->setItem(index, 0, new QTableWidgetItem(QString::fromStdString(s.getCod())));
    tabelSubstante->setItem(index, 1, new QTableWidgetItem(QString::fromStdString(s.getDenumire())));
    tabelSubstante->setItem(index, 2, new QTableWidgetItem(QString::fromStdString(s.getClasa())));
    tabelSubstante->setItem(index, 3, new QTableWidgetItem(QString::fromStdString(s.getClasificare())));
    if (!s.esteInterzis())
    {
        tabelSubstante->setItem(index, 4, new QTableWidgetItem(QString::number(s.getCantitLimitata())));
        tabelSubstante->setItem(index, 5, new QTableWidgetItem(QString::number(s.getGrupaTransport())));
    }
    else
    {
        tabelSubstante->setItem(index, 4, new QTableWidgetItem);
        tabelSubstante->setItem(index, 5, new QTableWidgetItem);
    }
}

ViewerWindow::~ViewerWindow()
{
    s.unsubscribe(this);
}

void ViewerWindow::init()
{
    setLayout(layMain);

    layMain->addWidget(textSus);
    layMain->addWidget(tabelSubstante);
    layMain->addWidget(textNr);

    tabelSubstante->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tabelSubstante->setColumnCount(6);
    tabelSubstante->setHorizontalHeaderItem(0, new QTableWidgetItem("Cod ONU"));
    tabelSubstante->setHorizontalHeaderItem(1, new QTableWidgetItem("Denumire substanta"));
    tabelSubstante->setHorizontalHeaderItem(2, new QTableWidgetItem("Clasa"));
    tabelSubstante->setHorizontalHeaderItem(3, new QTableWidgetItem("Cod clasificare"));
    tabelSubstante->setHorizontalHeaderItem(4, new QTableWidgetItem("Cantitate limitata"));
    tabelSubstante->setHorizontalHeaderItem(5, new QTableWidgetItem("Grupa transport"));

    layMain->addLayout(layIn);
    layMain->addLayout(layBtns);

    layIn->addWidget(new QLabel("Cauta dupa codul ONU"));
    layIn->addRow("Codul ONU: ", codIn);

    layBtns->addWidget(btnSearch,1,1);
    layBtns->addWidget(btnRefresh, 1, 2);
}

void ViewerWindow::connect()
{
    QObject::connect(btnSearch, &QPushButton::clicked, [&]() {cautaDupaCod(); });
    QObject::connect(btnRefresh, &QPushButton::clicked, [&]() {updateTable(); });
}

ViewerWindow::ViewerWindow(ServiceSubstante& s, QWidget* parent) :s{ s }, QWidget(parent)
{
    init();
    connect();
    updateTable();
    s.subscribe(this);
}

void CRUDWindow::init()
{
    setLayout(layMain);

    layMain->addLayout(layInput);
    layMain->addLayout(layBtns);

    codIn->setEditable(true);

    layInput->addRow("Cod ONU: ",codIn);
    layInput->addRow("Denumire: ", denumireIn);
    layInput->addRow("Clasa: ", clasaIn);
    layInput->addRow("Cod clasificare: ", clasifIn);
    layInput->addRow("Cantitate limitata: ", cantitLimitIn);
    layInput->addRow("Grupa transport: ", grTranspIn);
    layInput->addRow("Este transportul substantei interzis? ", checkInterzis);
    layInput->addRow("Nu face substanta parte din ADR?", checkNonADR);

    layBtns->addWidget(btnAdd,1,1);
    layBtns->addWidget(btnModify, 1, 2);
    layBtns->addWidget(btnRemove, 1, 3);

    for (const auto& s : claseDisponibile)
    {
        clasaIn->addItem(QString::fromStdString(s));
    }

    for (int i = 0; i <= 4; i++)
        grTranspIn->addItem(QString::number(i));
}

void CRUDWindow::connect()
{
    QObject::connect(btnAdd, &QPushButton::clicked, [&]() {add(); codIn->setFocus(); codIn->setCurrentText(""); });
    QObject::connect(btnModify, &QPushButton::clicked, [&]() {modify(); codIn->setFocus(); codIn->setCurrentText(""); });
    QObject::connect(btnRemove, &QPushButton::clicked, [&]() {remove(); codIn->setFocus(); codIn->setCurrentText(""); });
    QObject::connect(checkInterzis, &QCheckBox::stateChanged, [&]() {modificaFormularInterzis(); });
    QObject::connect(codIn, &QComboBox::currentTextChanged, [&]() {autoCompleteazaFormular(); });
    QObject::connect(checkNonADR, &QCheckBox::stateChanged, [&]() {modificaFormularInterzis(1); });
}

void CRUDWindow::add() const
{
    try
    {
        verificaCampuriInput();
        auto txtDenum = denumireIn->text().remove("\r").replace("\n"," ").toStdString();
        string sit;
        if (checkInterzis->isChecked())
            sit = "INTERZIS";
        else if (checkNonADR->isChecked())
            sit = "nonADR";
        else
            sit = "OK";
        s.add(codIn->currentText().toStdString(), txtDenum, clasaIn->currentItem()->text().toStdString(), clasifIn->text().toStdString(),stod(cantitLimitIn->text().toStdString()),grTranspIn->currentRow(), sit);
    }
    catch (const Eroare& e)
    {
        msjEroare(e.getError());
    }
}

void CRUDWindow::modify() const
{
    try
    {
        verificaCampuriInput();
        auto txtDenum = denumireIn->text().remove("\r").replace("\n", " ").toStdString();
        string sit;
        if (checkInterzis->isChecked())
            sit = "INTERZIS";
        else if (checkNonADR->isChecked())
            sit = "nonADR";
        else
            sit = "OK";
        s.modify(codIn->currentText().toStdString(), txtDenum, clasaIn->currentItem()->text().toStdString(), clasifIn->text().toStdString(),stod(cantitLimitIn->text().toStdString()),grTranspIn->currentRow(),sit);
    }
    catch (const Eroare& e)
    {
        msjEroare(e.getError());
    }
}

void CRUDWindow::remove() const
{
    try
    {
        if (codIn->currentText().isEmpty())
            throw UIError("Cod ONU vid!");
        s.remove(codIn->currentText().toStdString());
    }
    catch (const Eroare& e)
    {
        msjEroare(e.getError());
    }
}

void CRUDWindow::verificaCampuriInput() const
{
    string msj = "";
    if (codIn->currentText().isEmpty())
        msj += "Cod ONU vid!\n";
    if (denumireIn->text().isEmpty())
        msj += "Denumirea vida!\n";
    if (clasaIn->currentIndex().isValid() == false)
        msj += "Clasa neselectata!\n";
    if (clasifIn->text().isEmpty())
        msj += "Cod de clasificare vid!\n";
    if (!checkInterzis->isChecked())//Daca avem substanta interzisa, nu ne trebuie aceste amanunte
    {
        if (cantitLimitIn->text().isEmpty())
            msj += "Cantitate limitata vida!\n";
        if (grTranspIn->currentIndex().isValid() == false)
            msj += "Grupa de transport neselectata!\n";
    }
    if (!msj.empty())
        throw UIError(msj);
}

void CRUDWindow::modificaFormularInterzis(int sender) noexcept
{
    if (sender == 0)
    {
        if (checkInterzis->isChecked())//Se selecteaza checkInterzis
        {
            cantitLimitIn->setText("0");
            grTranspIn->setCurrentRow(0);
            cantitLimitIn->setEnabled(false);
            grTranspIn->setEnabled(false);
            checkNonADR->setEnabled(false);
            checkNonADR->setChecked(false);
        }
        else
        {
            cantitLimitIn->setEnabled(true);
            grTranspIn->setEnabled(true);
            checkNonADR->setEnabled(true);
        }
    }
    else if (sender == 1)
    {
        if (checkNonADR->isChecked())//Se selecteaza checkNonADR
        {
            cantitLimitIn->setText("0");
            grTranspIn->setCurrentRow(0);
            cantitLimitIn->setEnabled(false);
            grTranspIn->setEnabled(false);
            checkInterzis->setEnabled(false);
            checkInterzis->setChecked(false);
        }
        else
        {
            cantitLimitIn->setEnabled(true);
            grTranspIn->setEnabled(true);
            checkInterzis->setEnabled(true);
        }
    }
}

void CRUDWindow::autoCompleteazaFormular()
{
    try
    {
        auto subst = s.cautaCod(codIn->currentText().toStdString());
        denumireIn->setText(QString::fromStdString(subst.getDenumire()));
        for(int i=0;i<13;i++)
            if (subst.getClasa() == claseDisponibile[i])
            {
                clasaIn->setCurrentRow(i);
                break;
            }
        clasifIn->setText(QString::fromStdString(subst.getClasificare()));
        cantitLimitIn->setText(QString::number(subst.getCantitLimitata()));
        grTranspIn->setCurrentRow(subst.getGrupaTransport());
        checkInterzis->setChecked(subst.esteInterzis());
    }
    catch (const Eroare& e)
    {

    }
}

void CRUDWindow::actualizeazaCampCodONU()
{
    codIn->clear();
    auto all = s.getAllCodes();
    for (const auto& c : all)
        codIn->addItem(QString::fromStdString(c));
}

void CRUDWindow::updateSelf()
{
    actualizeazaCampCodONU();
}

CRUDWindow::CRUDWindow(ServiceSubstante& s, QWidget* parent) : s{ s }, QWidget{ parent }
{
    init();
    connect();
    s.subscribe(this);
    actualizeazaCampCodONU();
}

CRUDWindow::~CRUDWindow()
{
    s.unsubscribe(this);
}

void MyWindow::msjEroare(string msj) const
{
    QMessageBox::critical(nullptr, "Eroare", QString::fromStdString(msj));
}

void ScoreWindow::init()
{
    setLayout(layMain);

    layMain->addWidget(textSus);
    layMain->addWidget(tabelSubstante);
    layMain->addLayout(layIn);
    layMain->addLayout(layBtns);
    layMain->addWidget(textScor);
    layMain->addWidget(textGrCantitLimit);
    layMain->addWidget(textRezultat);

    tabelSubstante->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tabelSubstante->setColumnCount(6);
    tabelSubstante->setHorizontalHeaderItem(0, new QTableWidgetItem("Cod ONU"));
    tabelSubstante->setHorizontalHeaderItem(1, new QTableWidgetItem("Cantitatea totala"));
    tabelSubstante->setHorizontalHeaderItem(2, new QTableWidgetItem("Cantitatea per pachet"));
    tabelSubstante->setHorizontalHeaderItem(3, new QTableWidgetItem("Cantitate limitata a substantei"));
    tabelSubstante->setHorizontalHeaderItem(4, new QTableWidgetItem("Scor"));
    tabelSubstante->setHorizontalHeaderItem(5, new QTableWidgetItem("Cantitate limitata rezultata"));

    layBtns->addWidget(rmvRowBtn, 1, 3);
    layBtns->addWidget(calcBtn, 2, 2);
    layBtns->addWidget(addDataBtn, 1, 1);

    layIn->addRow("Codul ONU: ", codIn);
    layIn->addRow("Cantitatea totala: ", cantitTotalaIn);
    layIn->addRow("Cantitatea per pachet: ", cantitPachetIn);
}

void ScoreWindow::connect()
{
    QObject::connect(rmvRowBtn, &QPushButton::clicked, [&]() {removeRow(); });
    QObject::connect(calcBtn, &QPushButton::clicked, [&]() {calculate(); });
    QObject::connect(addDataBtn, &QPushButton::clicked, [&]() {addData(); });
}

void ScoreWindow::addRow()
{
    tabelSubstante->setRowCount(tabelSubstante->rowCount()+1);
}

void ScoreWindow::removeRow()
{
    tabelSubstante->setRowCount(tabelSubstante->rowCount()-1);
    sT.pop();
}

void ScoreWindow::calculate()
{
    sT.calculeaza();
    for (int i = 0; i < tabelSubstante->rowCount(); i++)
    {
        try
        {
            auto rezRand = sT.calculeazaIndex(i);
            tabelSubstante->setItem(i, 4, new QTableWidgetItem(QString::number(rezRand.second)));
            tabelSubstante->setItem(i, 5, new QTableWidgetItem(QString::number(rezRand.first)));
            coloreazaRandAtentionare(i, rezRand);
        }
        catch (Eroare& e)
        {
            msjEroare(e.getError());
        }
    }
    textGrCantitLimit->setText("Greutatea pentru cantitati limitate este: " + QString::number(sT.getGreutateCantitLimitate())+" kg.");
    textScor->setText("Scorul este: "+QString::number(sT.getScor())+" puncte.");
    auto stare = sT.getSituatie();
    if (stare == 0)
    {
        textRezultat->setText("Nu este necesara nicio placuta!");
    }
    else if (stare == 1)
    {
        textRezultat->setText("Se coboara placuta alb/negru!");
    }
    else if (stare == 2)
    {
        textRezultat->setText("Se coboara placuta portocalie!");
    }
    else if (stare == 3)
    {
        textRezultat->setText("Sunt substante interzise! Nu se poate efectua calculul!");
    }
}

void ScoreWindow::addData()
{
    verificaCampuriInput();
    addRow();
    auto index = tabelSubstante->rowCount() - 1;
    tabelSubstante->setItem(index, 0, new QTableWidgetItem(codIn->text()));
    tabelSubstante->setItem(index, 1, new QTableWidgetItem(cantitTotalaIn->text()));
    tabelSubstante->setItem(index, 2, new QTableWidgetItem(cantitPachetIn->text()));
    auto cod = codIn->text().toStdString();
    try
    {
        auto subst = s.cautaCod(cod);
        auto cT = cantitTotalaIn->text().toDouble();
        auto cP = cantitPachetIn->text().toDouble();
        tabelSubstante->setItem(index, 3, new QTableWidgetItem(QString::number(subst.getCantitLimitata())));
        sT.add(subst, cP, cT);
    }
    catch (const Eroare& ex)
    {
        msjEroare(ex.getError());
        tabelSubstante->setRowCount(0);
    }
}

void ScoreWindow::coloreazaRandAtentionare(int index, pair<double, double> info) const
{
    if (sT.getIndecsiSubstanteNonADR().find(index) != sT.getIndecsiSubstanteNonADR().end())//Avem substanta care nu face parte din ADR
    {
        for (int i = 0; i < tabelSubstante->columnCount(); i++)
            tabelSubstante->item(index, i)->setBackgroundColor(culoareGasit);
    }
    if (sT.getIndecsiSubstanteInterzise().find(index) != sT.getIndecsiSubstanteInterzise().end())//Daca avem substanta interzisa
    {
        for (int i = 0; i < tabelSubstante->columnCount(); i++)
            tabelSubstante->item(index, i)->setBackgroundColor(culoareInterzis);
    }
    else if (info.second != 0)//Daca avem scorul diferit de 0
    {
        tabelSubstante->item(index, 4)->setBackgroundColor(culoareScor);
    }
    else if (info.first != 0)//Daca avem greutate limitata mai mare de 0
    {
        tabelSubstante->item(index, 5)->setBackgroundColor(QColor(0,0,0));
        tabelSubstante->item(index, 5)->setTextColor(QColor(255,255,255));
    }
}

void ScoreWindow::verificaCampuriInput() const
{
    string msj = "";
    if (codIn->text().isEmpty())
        msj += "Cod ONU este vid!\n";
    if (cantitPachetIn->text().isEmpty())
        msj += "Cantitatea per pachet este vida!\n";
    if (cantitTotalaIn->text().isEmpty())
        msj += "Cantitatea totala este vida!\n";
}

ScoreWindow::ScoreWindow(ServiceSubstante& s, QWidget* parent) :s{ s }, QWidget(parent)
{
    init();
    connect();
}
