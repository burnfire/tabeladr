#pragma once
#include "Eroare.h"
#include "Serializator.h"

using std::string;
const string claseDisponibile[] = {"1","2","3","4.1","4.2","4.3","5.1","5.2","6.1",
"6.2","7","8","9"};
const int multiplGrTransp[] = { 99999,50,3,1,0 };

class Substanta : public Serializabil
{
private:
	friend class ValidatorSubstanta;
	string cod;//codul ONU
	string denumire;//denumirea substantei
	string clasa;
	string clasif;//codul de clasificare
	double cantitLimit;//cantitate limitata
	int grTransp;//grupa transport
	string situatie;/*situatia substantei, daca este cazul, astfel:
					   -transport interzis => INTERZIS
					   -nu este ADR => nonADR
					   -niciuna => OK*/
public:
	/*validare - valideaza substanta curenta
	in: -
	out: true/false, daca datele substantei sunt corecte sau nu*/
	bool validare() const;
	/*Substanta - constructor cu parametri
	in: cod:string, denumire:string, clasa:string, clasificare:string, cantitate limitata:double, grupa transport:int, situatie:string(default="OK")
	out: -
	raises: DomainError, daca datele introduse nu sunt corecte*/
	Substanta(string cod, string denumire, string clasa, string clasif, double cantitLimit, int grTransp, string situatie="OK") :cod{ cod }, denumire{ denumire }, clasa{ clasa }, clasif{ clasif }, cantitLimit{ cantitLimit }, grTransp{ grTransp }, situatie{ situatie }
	{
		if (!validare())
			throw DomainError("Datele introduse nu sunt corecte!");
	}
	/*Substanta(Substanta) - copy constructor
	in: s:Substanta
	out: -*/
	Substanta(const Substanta& s) :cod{ s.cod }, denumire{ s.denumire }, clasa{ s.clasa }, clasif{ s.clasif }, cantitLimit{ s.cantitLimit }, grTransp{ s.grTransp }, situatie{ s.situatie }{}
	/*getCod - obtine codul ONU
	in: -
	out: cod:string*/
	const string& getCod() const;
	/*getDenumire - obtine denumirea substantei
	in: -
	out: denumire:string*/
	const string& getDenumire() const;
	/*getClasa - obtine clasa substantei
	in: -
	out: clasa:string*/
	const string& getClasa() const;
	/*getClasificare - obtine codul de clasificare al substantei
	in: -
	out: clasificare:string*/
	const string& getClasificare() const;
	/*getCantitLimitata - obtine cantitatea limitata a substantei
	in: -
	out: cantitateLimitata:double*/
	const double& getCantitLimitata() const;
	/*getGrupaTransport - obtine grupa de transport a substantei
	in: -
	out: grupaTransport:int*/
	const int& getGrupaTransport() const;
	/*esteInterzis - verifica daca transportul substantei este interzis sau nu
	in: -
	out: interzis:bool, unde True inseamna ca transportul substantei este interzis*/
	const bool esteInterzis() const;
	/*esteNonADR - verifica daca transportul substantei face obiectul ADR
	in: -
	out: rez:bool, unde True inseamna ca transportul nu face obiectul ADR*/
	const bool esteNonADR() const;
	/*serialize - mostenita din Serializabil*/
	string serialize() const override;
	/*criterii de sortare*/
	//dupa cod
	static bool critCod(Substanta a, Substanta b);
	//dupa denumire
	static bool critDenumire(Substanta a, Substanta b);
};

class DeserializatorSubstanta
{
public:
	/*
	deserialize - Ia un sir de caractere anume si il transforma in obiectul corespunzator.
	in: sir:string
	out: obj:shared_ptr<Serializabil>
	raises: DomainError, daca datele din sirul de caractere nu pot construi un obiect valid
	Forma serializata a unei substante este:
	Cod ONU; Denumire; Clasa; Clasificare; Cantitate limitata; Grupa de transport; situatie (OK, INTERZIS sau nonADR)*/
	static Substanta deserialize(const std::string sir);
};
