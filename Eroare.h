#pragma once
#include <string>
using namespace std;

class Eroare
{
protected:
	string mesaj;//Mesajul de eroare
public:
	Eroare(string msj) :mesaj{ "Error: "+msj } {}
	/*getError - obtine mesajul de eroare
	in: -
	out: eroare:string*/
	string getError() const;
};

class DomainError : public Eroare
{
public:
	DomainError(string msj) : Eroare{ msj } { mesaj = "Domain " + mesaj; }
};

class RepoError : public Eroare
{
public:
	RepoError(string msj) :Eroare{ msj } { mesaj = "Repo " + mesaj; }
};

class ServiceError : public Eroare
{
public:
	ServiceError(string msj) :Eroare{ msj } { mesaj = "Service " + mesaj; }
};

class UIError : public Eroare
{
public:
	UIError(string msj) :Eroare{ msj } { mesaj = "U.I. " + mesaj; }
};

