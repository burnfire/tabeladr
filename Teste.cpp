#include "Teste.h"

void testAll()
{
	set1();
	set2();
}

void set1()
{
	testDomain();
	testRepo();
	testService();
	//qDebug() << (_CrtDumpMemoryLeaks() ? "S-au detectat memory leaks!" : "Nu s-au detectat memory leaks!");
}

void set2()
{
	/*Teste domain*/
	try
	{
		Substanta s{ "","","","",0,0 };
		assert(false);
	}
	catch (const Eroare& ex)
	{
		assert(true);
	}
	try
	{
		Substanta s{ "0004","Dada","x","y",0,0 };
		assert(false);
	}
	catch (const Eroare& ex)
	{
		assert(true);
	}
	try
	{
		Substanta s{ "0004","Picrat de amoniu","1","1.4D",0,0,"BLA" };
		assert(false);
	}
	catch (const Eroare& ex)
	{
		assert(true);
	}
	try
	{
		Substanta s{ "0004","Picrat de amoniu","1","1.4D",0,-1 };
		assert(false);
	}
	catch (const Eroare& ex)
	{
		assert(true);
	}
	try
	{
		Substanta s{ "0004","Picrat de amoniu","1","1.4D",-1,0 };
		assert(false);
	}
	catch (const Eroare& ex)
	{
		assert(true);
	}
	Substanta subst{ "0004","Picrat de amoniu","1","1.4D",1,0 };
	assert(subst.getCod() == "0004");
	assert(subst.getDenumire() == "Picrat de amoniu");
	assert(subst.getClasa() == "1");
	assert(subst.getClasificare() == "1.4D");
	assert(subst.getGrupaTransport() == 0);
	assert(subst.getCantitLimitata() == 1);
	assert(subst.esteInterzis() == false);
	assert(subst.esteNonADR() == false);
	assert(subst.serialize() == "0004;Picrat de amoniu;1;1.4D;1.000000;0;OK");
	assert(DeserializatorSubstanta::deserialize("0004;Picrat de amoniu;1;1.4D;1;0;OK").getDenumire() == "Picrat de amoniu");
	assert(Substanta::critCod(DeserializatorSubstanta::deserialize("0004;Picrat de amoniu;1;1.4D;1.000000;0;OK"), DeserializatorSubstanta::deserialize("0005;Picrat de amoniu;1;1.4D;1.000000;0;OK")));
	/*Testele privind repo*/
	RepoSubstanta repo;
	repo.add("0004", Substanta("0004", "Picrat de amoniu", "1", "1.4D", 0, 0));
	repo.add("0005", Substanta("0005", "Oxid de dihidrogen", claseDisponibile[3], "4", 0, 4, "INTERZIS"));
	assert(repo.size() == 2);
	try
	{
		repo.modify("0006", Substanta("0006", "Ceva gresit", "1", "1.4D", 0, 0));
		assert(false);
	}
	catch (const Eroare& ex)
	{
		assert(true);
	}
	assert(repo.getAll()[1].getCod() == "0005");
	assert(repo.size() == 2);
	repo.remove("0005");
	assert(repo.size() == 1);
	try
	{
		repo.remove("0005");
		assert(false);
	}
	catch (const Eroare& ex)
	{
		assert(true);
	}
}

void testDomain()
{
	testDomainErori();
	testDomainSubstante();
	testSerial();
}

void testSerial()
{
	Substanta s("0005", "Cartuse pentru arme cu incarcatura exploziva", "1", "1.1F",0,1);
	assert(s.serialize() == "0005;Cartuse pentru arme cu incarcatura exploziva;1;1.1F;0.000000;1;OK");
	assert(DeserializatorSubstanta::deserialize("0005;Cartuse pentru arme cu incarcatura exploziva;1;1.1F;0;1;OK").getDenumire() == "Cartuse pentru arme cu incarcatura exploziva");
	assert(DeserializatorSubstanta::deserialize("0020;MUNITII TOXICE cu incarcatura de dispersie;1;1.2K;;;INTERZIS").esteInterzis() == true);
}

void testDomainErori()
{
	unique_ptr<Eroare> e;//Polimorfism
	//Eroare simpla
	e = make_unique<Eroare>("O eroare random");
	assert(e->getError() == "Error: O eroare random");
	e = make_unique<DomainError>("O eroare de domeniu");
	assert(e->getError() == "Domain Error: O eroare de domeniu");
	e = make_unique<RepoError>("O eroare de repo");
	assert(e->getError() == "Repo Error: O eroare de repo");
	e = make_unique<ServiceError>("O eroare de service");
	assert(e->getError() == "Service Error: O eroare de service");
}

void testDomainSubstante()
{
	unique_ptr<Substanta> s;
	try
	{
		s = make_unique<Substanta>("", "", "", "",0,1);
		assert(false);
	}
	catch (const DomainError& e)
	{
		assert(true);
	}
	try
	{
		s = make_unique<Substanta>("ff", "ff", "sdfsfd", "sdvsdf",0,0);
		assert(false);
	}
	catch (const DomainError& e)
	{
		assert(true);
	}
	try
	{
		s = make_unique<Substanta>("ff", "ff", "1", "ff",0,0);
		assert(true);
	}
	catch (const DomainError& e)
	{
		assert(false);
	}
	assert(Substanta::critCod(Substanta("0005", "Cartuse pentru arme cu incarcatura exploziva", "1", "1.1F",0,1), Substanta("0006", "Picrat de amoniu", "1", "1.1F",0,1)));
	assert(Substanta::critDenumire(Substanta("0004", "AAA", "1", "1.1D",0,1), Substanta("0004", "AAB", "1", "1.1D",0,1)));
}

void testRepo()
{
	RepoSubstanta repo;
	assert(repo.size()==0);
	try
	{
		repo["0004"];
		assert(false);
	}
	catch (const RepoError ex)
	{
		assert(true);
	}
	repo.add("0004", Substanta("0004","Picrat de amoniu","1","1.1D",0,1));
	assert(repo.size() == 1);
	assert(repo.getAll().size() == 1);
	assert(repo["0004"].getDenumire() == "Picrat de amoniu");
	repo.modify("0004", Substanta("0004", "Oxid de dihidrogen", "1", "1.1F",0,1));
	assert(repo["0004"].getDenumire() == "Oxid de dihidrogen");
	assert(repo.size() == 1);
	repo.remove("0004");
	assert(repo.size() == 0);
	try
	{
		repo["0004"];
		assert(false);
	}
	catch (const RepoError ex)
	{
		assert(true);
	}
	FileRepoSubstanta file("test.csv");
	assert(file.size() == 0);
	try
	{
		file["0004"];
		assert(false);
	}
	catch (const RepoError& ex)
	{
		assert(true);
	}
	file.add("0004", Substanta("0004", "Picrat de amoniu", "1", "1.1D",0,1));
	assert(file.size() == 1);
	assert(file["0004"].getDenumire() == "Picrat de amoniu");
	file.modify("0004", Substanta("0004", "Monoxid de dihidrogen", "1", "1.1D",0,1));
	assert(file["0004"].getDenumire() == "Monoxid de dihidrogen");
	assert(file.size() == 1);
	file.remove("0004");
	assert(file.size() == 0);
	try
	{
		file["0004"];
		assert(false);
	}
	catch (const RepoError& ex)
	{
		assert(true);
	}
}

void testService()
{
	RepoSubstanta repo;
	ServiceSubstante s{ repo };
	s.add("0004", "Pircat de amoniu", "1", "1.1D",0,1);
	s.add("0005", "Cartuse de arme", "1", "1.1F",0,1);
	s.add("0124", "Perforatoare cu incarcatura cumulativa", "1", "1.1D",0,1);
	assert(s.getAll().size() == 3);
	try
	{
		s.add("0004", "Ceva", "1", "1.1D",0,1);
		assert(false);
	}
	catch (const RepoError& ex)
	{
		assert(true);
	}
	s.modify("0004", "Picrat de amoniu", "1", "1.1D",0,1);
	try
	{
		s.modify("0000", "Ceva", "1", "1.1D",0,1);
		assert(false);
	}
	catch (const RepoError& ex)
	{
		assert(true);
	}
	s.remove("0124");
	try
	{
		s.remove("0124");
		assert(false);
	}
	catch (const RepoError& ex)
	{
		assert(true);
	}
	assert(s.cautaCod("0004").getDenumire() == "Picrat de amoniu");
	assert(s.sort(Substanta::critCod,true)[0].getCod()=="0005");
	testServiceScoruri();
}

void testServiceScoruri()
{
	ServiceTransportabilitate s;
	s.add(Substanta("0005", "Da", "1", "1.1F", 0.5, 3), 0.4, 7999);
	s.calculeaza();
	assert(s.getSituatie() == 0);
	s.add(Substanta("0005", "Da", "1", "1.1F", 0.5, 3), 0.4, 2);
	s.calculeaza();
	assert(s.getSituatie() == 1);
	assert(s.getGreutateCantitLimitate() == 8001);
	assert(s.calculeazaIndex(0).first == 7999);
	s.add(Substanta("0005", "Da", "1", "1.1F", 0.5, 3), 1, 500);
	s.calculeaza();
	assert(s.getSituatie() == 1);
	assert(s.getScor() == 500);
	assert(s.calculeazaIndex(2).second == 500);
	s.add(Substanta("0005", "Da", "1", "1.1F", 0.5, 3), 1, 501);
	s.calculeaza();
	assert(s.getSituatie() == 2);
	assert(s.getScor() == 1001);
	s.add(Substanta("0020", "Munitii toxice", "1", "1.1K", 1, 1, "INTERZIS"), 1, 1);
	s.calculeaza();
	assert(s.getSituatie() == 3);
	assert(s.getIndecsiSubstanteInterzise().find(4) != s.getIndecsiSubstanteInterzise().end());
}
