#pragma once
#include <set>
using namespace std;
class Observator
{
public:
	/*updateSelf - updateaza starea observatorului curent. Metoda pur virtuala*/
	virtual void updateSelf() = 0;
};

class Subiect
{
private:
	set<Observator*> obs;
public:
	/*subscribe - adauga un observator.
	in: o:Observator*
	out: -*/
	void subscribe(Observator* o);
	/*notify - notifica observatorii ca s-a modificat ceva la subiect.
	in: -
	out: -*/
	void notify();
	/*unsubscribe - sterge un observator. Metoda pur virtuala
	in: o:Observator*
	out: -*/
	void unsubscribe(Observator* o);
};

