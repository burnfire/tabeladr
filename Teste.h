#pragma once
#include "Eroare.h"
#include "Substanta.h"
#include <cassert>
#include <memory>
#include <qdebug.h>
#include "RepoAbstract.h"
#include "Service.h"

void testAll();
void set1();
void set2();
void testDomain();
void testSerial();
void testDomainErori();
void testDomainSubstante();
void testRepo();
void testService();
void testServiceScoruri();

