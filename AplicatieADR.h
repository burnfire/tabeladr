#pragma once

#include <QtWidgets/QWidget>
#include <qgridlayout.h>
#include <QPushButton>
#include <QLabel>
#include "RepoAbstract.h"
#include "Service.h"
#include <QTableWidget>
#include <memory>
#include <qformlayout.h>
#include <qlineedit.h>
#include <qlistwidget.h>
#include <qmessagebox.h>
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qapplication.h>
#include <qgroupbox.h>

class MyWindow
{
protected:
    /*Standardizam culorile*/
    QColor culoareInterzis = QColor::fromRgb(255, 0, 0);
    QColor culoareGasit = QColor::fromRgb(0, 255, 0);
    QColor culoareScor = QColor::fromRgb(255, 137, 0);
    /*init - functia care aranjeaza interfata*/
    virtual void init()=0;
    /*connect - functia care conecteaza socketurile*/
    virtual void connect()=0;
    /*msjEroare - afiseaza un mesaj de eroare
    in: msj:string
    out: -*/
    void msjEroare(string msj) const;
    /*verificaCampuriInput - face o verificare a tuturor campurilor de input din fereastra*/
    virtual void verificaCampuriInput() const=0;
};

class ViewerWindow : public QWidget, public MyWindow, public Observator
{
private:
    ServiceSubstante& s;
    QTableWidget* tabelSubstante = new QTableWidget;
    QVBoxLayout* layMain = new QVBoxLayout;
    QLabel* textSus = new QLabel{ "Aici vezi toate substantele ADR disponibile" };
    QLabel* textNr = new QLabel{ "Sunt {0} substante." };
    QLineEdit* codIn = new QLineEdit;
    QFormLayout* layIn = new QFormLayout;
    QGridLayout* layBtns = new QGridLayout;
    QPushButton* btnSearch = new QPushButton{ "Cauta" };
    QPushButton* btnRefresh = new QPushButton{ "Refresh" };
    void init() override;
    void connect() override;
public:
    ViewerWindow(ServiceSubstante& s, QWidget* parent = Q_NULLPTR);
    /*updateTable - updateaza tabelul cu substante*/
    void updateTable();
    /*updateSelf - din Observator. Va updata tabelul*/
    void updateSelf() override;
    /*cautaDupaCod - cauta o substanta dupa codul ONU introdus si il afiseaza in tabel*/
    void cautaDupaCod();
    /*formateazaRand - coloreaza cu rosu randul care contine o substanta care nu poate fi transportata ADR
    in: indexRand:int, substanta:Substanta&
    out: -*/
    void formateazaRand(int index, const Substanta& s);
    /*verificaCampuriInput - verifica daca la apasarea butonului de cautare s-a introdus un cod ONU
    in: -
    out: -
    raises: UIError:
                    -daca campul de introducere a codului este vid*/
    void verificaCampuriInput() const override;
    /*completeazaRand - in functie de o substanta, completeaza un rand din tabel
    Se presupune ca numarul de randuri din tabel este deja setat
    in: index:int, s:Substanta&
    out: -*/
    void completeazaRand(int index, const Substanta& s);
    ~ViewerWindow();
};

class AplicatieADR : public QWidget, public MyWindow
{
    Q_OBJECT

public:
    AplicatieADR(ServiceSubstante& s, QWidget* parent = Q_NULLPTR);

private:
    ServiceSubstante& serviceSubstanta;
    void init() override;
    void connect() override;
    /*showViewer - functia care afiseaza fereastra de afisare a substantelor*/
    void showViewer() const;
    /*showCRUD - functia care afiseaza fereastra de modificare a substantelor*/
    void showCRUD() const;
    /*showScoreWindow - functia care afiseaza fereastra de calculare a scorului ADR*/
    void showScoreWindow() const;
    void verificaCampuriInput() const override;
    QVBoxLayout* layMain = new QVBoxLayout;
    QGridLayout* layBtns = new QGridLayout;
    QPushButton* btnView = new QPushButton{ "Vezi substantele" };
    QPushButton* btnCRUD = new QPushButton{ "Modifica substante" };
    QPushButton* btnScore = new QPushButton{ "Verifica scorul ADR" };
    QLabel* textSus = new QLabel{ "Acesta este tabelul ADR interactiv. Alege o optiune de mai jos:" };
    QGroupBox* boxLegend = new QGroupBox;
    QGridLayout* layBoxLegend = new QGridLayout;
    QLabel* txtCulInterzis = new QLabel{ "Substantele care au culoarea asta, sunt interzise transportului." };
    QLabel* txtCulOK = new QLabel{ "Substantele care au culoarea asta, nu fac parte din acordul ADR. Pot fi transportate fara restrictii!" };
    QLabel* txtCulScor = new QLabel{ "Substantele colorate astfel contribuie cu un anumit scor datorita grupei de transport." };
    QLabel* txtCulCantitLimit = new QLabel{ "Substantele formatate astfel contribuie cu o anumita cantitate limitata." };
};

class CRUDWindow : public MyWindow, public QWidget, public Observator
{
private:
    ServiceSubstante& s;
    QVBoxLayout* layMain = new QVBoxLayout;
    QFormLayout* layInput = new QFormLayout;
    QGridLayout* layBtns = new QGridLayout;
    QComboBox* codIn = new QComboBox;
    QLineEdit* denumireIn = new QLineEdit;
    QListWidget* clasaIn = new QListWidget;
    QLineEdit* clasifIn = new QLineEdit;
    QPushButton* btnAdd = new QPushButton{ "&Adauga" };
    QPushButton* btnModify = new QPushButton{ "&Modifica" };
    QPushButton* btnRemove = new QPushButton{ "&Sterge" };
    QLineEdit* cantitLimitIn = new QLineEdit;
    QListWidget* grTranspIn = new QListWidget;
    QCheckBox* checkInterzis = new QCheckBox;
    QCheckBox* checkNonADR = new QCheckBox;
    void init() override;
    void connect() override;
    /*add - adauga o substanta*/
    void add() const;
    /*modify - modifica o substanta*/
    void modify() const;
    /*remove - sterge o substanta*/
    void remove() const;
    /*verificaCampuriInput - verifica daca datele de intrare furnizate de utilizator sunt corecte
    in: -
    out: -
    raises: UIError, daca exista un camp care contine datele de intrare cerute, astfel:
            -Cod ONU vid
            -Denumire vida
            -Clasa neselectata
            -Cod de clasificare vid
            Si, daca avem substanta care se poate transporta:
            -Cantitate limitata vida
            -Grupa de transport neselectata*/
    void verificaCampuriInput() const override;
    /*modificaFormularInterzis - daca este selectat checkInterzis, se golesc campurile cantitate limitata si grupa de transport si se dezactiveaza. Daca se reselecteaza, se reactiveaza campurile
                               - daca se selecteaza checkNonADR, se intampla acelasi lucru ca mai sus, doar ca se deselecteaza si checkInterzis
    in: sender:int, unde 0 e checkInterzis(default), si 1 e checkNonADR
    out: -*/
    void modificaFormularInterzis(int sender=0) noexcept;
    /*autoCompleteazaFormular - la apelul functiei, se incearca completarea campurilor de input, in caz ca inputul pentru cod ONU contine un cod valid
    in: -
    out: -*/
    void autoCompleteazaFormular();
    /*actualizeazaCampCodONU - functia care va actualiza lista de coduri ONU
    in: -
    out: -*/
    void actualizeazaCampCodONU();
    /*updateSelf - din Observator
    in: -
    out: -*/
    void updateSelf() override;
public:
    CRUDWindow(ServiceSubstante& s, QWidget* parent = Q_NULLPTR);
    ~CRUDWindow();
};

class ScoreWindow : public MyWindow, public QWidget
{
private:
    ServiceSubstante& s;
    ServiceTransportabilitate sT;
    QVBoxLayout* layMain = new QVBoxLayout;
    QFormLayout* layIn = new QFormLayout;
    QGridLayout* layBtns = new QGridLayout;
    QLabel* textSus = new QLabel{ "Aici calculezi scorul ADR si vezi cum trebuie pancardat vehiculul." };
    QTableWidget* tabelSubstante = new QTableWidget;
    QPushButton* rmvRowBtn = new QPushButton{ "Sterge ultimul rand" };
    QPushButton* calcBtn = new QPushButton{ "Calculeaza" };
    QLineEdit* codIn = new QLineEdit;
    QLineEdit* cantitTotalaIn = new QLineEdit;
    QLineEdit* cantitPachetIn = new QLineEdit;
    QLabel* textScor = new QLabel{ "Scorul este: " };
    QLabel* textGrCantitLimit = new QLabel{ "Greutatea pentru cantitati limitate este: " };
    QLabel* textRezultat = new QLabel{ "Nu se afiseaza nicio placuta!" };
    QPushButton* addDataBtn = new QPushButton{ "Adauga date in tabel" };
    void init() override;
    void connect() override;
    /*addRow - adauga un rand la tabel*/
    void addRow();
    /*removeRow - sterge ultimul rand din tabel*/
    void removeRow();
    /*calculate - efectueaza calculele la apasarea butonului corespunzator*/
    void calculate();
    /*addData - adauga datele din campurile de intrare in tabel*/
    void addData();
    /*coloreazaRandAtentionare - coloreaza un rand din tabel in asa fel incat sa alerteze utilizatorul ca se intalneste o situatie speciala
    Formatarea celulelor Scor si Cantitate limitata rezultata se va face astfel:
    substanta nu face parte din ADR => se coloreaza randul cu verde
    substanta este interzisa => se coloreaza randul rosu
    scor>0 => Celula de pe coloana Scor cu fundal portocaliu
    cantitLimit>0 => Celula de pe coloana Cantitate limitata rezultata cu fundal negru si scris alb
    in: indexRand:int, informatiiFormatare:pair<double,double>, (greutate,scor)
    out: -*/
    void coloreazaRandAtentionare(int index, pair<double,double> info) const;
    /*verificaCampuriInput - verifica daca campurile de input necesare adaugarii unei substante in tabel sunt completate
    in: -
    out: -
    raises: UIError, daca:
                            -Cod ONU este vid
                            -Cantitatea per pachet este vida
                            -Cantitatea totala este vida*/
    void verificaCampuriInput() const override;
public:
    ScoreWindow(ServiceSubstante& s, QWidget* parent = Q_NULLPTR);
};