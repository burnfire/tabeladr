#include "Service.h"

void ServiceSubstante::add(string cod, string denumire, string clasa, string clasif, double cantitLimit, int grTransp, string situatie)
{
	Substanta s{ cod,denumire,clasa,clasif,cantitLimit,grTransp,situatie };
	repo.add(cod, s);
	notify();
}

void ServiceSubstante::modify(string cod, string denumire, string clasa, string clasif, double cantitLimit, int grTransp, string situatie)
{
	Substanta s{ cod,denumire,clasa,clasif, cantitLimit, grTransp,situatie };
	repo.modify(cod, s);
	notify();
}

void ServiceSubstante::remove(string cod)
{
	repo.remove(cod);
	notify();
}

vector<Substanta> ServiceSubstante::getAll() const
{
	return repo.getAll();
}

Substanta ServiceSubstante::cautaCod(string cod) const
{
	return repo[cod];
}

vector<Substanta> ServiceSubstante::sort(bool(*f)(Substanta a, Substanta b), bool reverse)
{
	vector<Substanta> rez = repo.getAll();
	std::sort(rez.begin(), rez.end(), f);
	if (reverse)
		std::reverse(rez.begin(), rez.end());
	return rez;
}

vector<string> ServiceSubstante::getAllCodes() const
{
	vector<string> rez;
	auto all = getAll();
	transform(all.begin(), all.end(), back_inserter(rez), [](const auto& p)
		{
			return p.getCod();
		});
	return rez;
}

void ServiceTransportabilitate::add(const Substanta& s, double cp, double ct)
{
	all.push_back(DTOScor(s, cp, ct));
}

void ServiceTransportabilitate::clear()
{
	all.clear();
	indecsiInterzise.clear();
	indecsiNonADR.clear();
	greutate = scor = 0;
	situatie = -1;
}

double ServiceTransportabilitate::getGreutateCantitLimitate() const
{
	return greutate;
}

double ServiceTransportabilitate::getScor() const
{
	return scor;
}

int ServiceTransportabilitate::getSituatie() const
{
	return situatie;
}

void ServiceTransportabilitate::calculeaza()
{
	scor = greutate = 0;
	situatie = 0;
	indecsiInterzise.clear();
	indecsiNonADR.clear();
	for (int i=0;i<all.size();i++)
	{
		if (get<0>(all[i]).esteNonADR())
		{
			indecsiNonADR.emplace(i);
		}
		else if (get<0>(all[i]).esteInterzis())
		{
			situatie = 3;
			indecsiInterzise.emplace(i);
		}
		else
		{
			auto rez = calculeazaIndex(i);
			greutate += rez.first;
			scor += rez.second;
		}
	}
	if (situatie == 3)
	{
		scor = greutate = 0;
	}
	else if (scor > 1000)
		situatie = 2;
	else if (greutate > 8000)
		situatie = 1;
}

pair<double, double> ServiceTransportabilitate::calculeazaIndex(int index) const
{
	auto elem = all[index];
	if (get<0>(elem).esteNonADR())
		return pair<double, double>(0, 0);
	double gr = 0, sc = 0;
	auto cL = get<1>(elem);
	auto cT = get<2>(elem);
	if (cL <= get<0>(elem).getCantitLimitata())
		gr += cT;
	else/* if (multiplGrTransp[get<0>(elem).getGrupaTransport()] * cT <= 1000)*/
		sc += multiplGrTransp[get<0>(elem).getGrupaTransport()] * cT;
	return pair<double, double>(gr, sc);
}

void ServiceTransportabilitate::pop()
{
	if(all.size()!=0)
		all.erase(all.end() - 1);
}

const set<int>& ServiceTransportabilitate::getIndecsiSubstanteInterzise() const noexcept
{
	return indecsiInterzise;
}

const set<int>& ServiceTransportabilitate::getIndecsiSubstanteNonADR() const noexcept
{
	return indecsiNonADR;
}
