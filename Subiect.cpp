#include "Subiect.h"

void Subiect::subscribe(Observator* o)
{
	obs.insert(o);
}

void Subiect::notify()
{
	for (const auto& x : obs)
		x->updateSelf();
}

void Subiect::unsubscribe(Observator* o)
{
	obs.erase(o);
}
