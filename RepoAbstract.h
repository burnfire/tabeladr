#pragma once

#include "Substanta.h"
#include "Serializator.h"
#include <map>
#include <memory>
#include <fstream>
#include <algorithm>

using namespace std;

/*Punem conditia ca obiectul sa poata fi serializabil, pentru memorarea in fisier/baza de date*/
template <class T>
class RepoAbstract
{
	/*Clasa abstracta. Ea defineste cum arata un repo in aplicatie*/
protected:
	map<string, T> all;
	/*exists - verifica daca o anumita cheie exista
	in: cheie:string
	out: true/false, daca exista sau nu cheia respectiva*/
	bool exists(string cheie) const;
public:
	/*add - adauga un obiect in repo
	in: cheie:string, obiect:T
	out: -
	raises: RepoError, daca obiectul cu o anumita cheie exista deja*/
	virtual void add(string cheie, T obiect) = 0;
	/*modify - modifica un obiect din repo
	in: cheie:string, obiectNou:T
	out: -
	raises: RepoError, daca obiectul cu o anumita cheie nu exista*/
	virtual void modify(string cheie, T obiect) = 0;
	/*remove - sterge un obiect din repo dupa cheie
	in: cheie:string
	out: -
	raises: RepoError, daca cheia nu exista*/
	virtual void remove(string cheie) = 0;
};

class RepoSerializabil : public RepoAbstract<shared_ptr<Serializabil>>
{
public:
	/*add - adauga un obiect in repo
	in: cheie:string, obiect:T
	out: -
	raises: RepoError, daca obiectul cu o anumita cheie exista deja*/
	virtual void add(string cheie, shared_ptr<Serializabil> obiect) override;
	/*modify - modifica un obiect din repo
	in: cheie:string, obiectNou:T
	out: -
	raises: RepoError, daca obiectul cu o anumita cheie nu exista*/
	virtual void modify(string cheie, shared_ptr<Serializabil> obiect) override;
	/*remove - sterge un obiect din repo dupa cheie
	in: cheie:string
	out: -
	raises: RepoError, daca cheia nu exista*/
	virtual void remove(string cheie) override;
};

class RepoSubstanta : public RepoSerializabil
{
	//Memoreaza substantele doar in memorie
public:
	virtual void add(string cheie, Substanta obiect);
	virtual void modify(string cheie, Substanta obiect);
	virtual void remove(string cheie) override;
	/*operator[] - returneaza obiectul aflat la o anumita cheie
	in: cheie:string
	out: obiect:T
	raises: RepoError, daca nu exista cheia*/
	Substanta operator[](string cheie);
	int size() const;
	/*getAll - obtine toate substantele din repo
	in: -
	out: all:vector<Substanta>*/
	vector<Substanta> getAll();
};

class FileRepoSubstanta : public RepoSubstanta
{
protected:
	string path;
	/*load - incarca din fisier datele memorate
	in: -
	out: -*/
	void load();
	/*save - salveaza toate substantele in fisier
	in: -
	out: -*/
	void save();
public:
	FileRepoSubstanta(string path) :path{ path } { load(); }
	void add(string cheie, Substanta obiect) override;
	void modify(string cheie, Substanta obiect) override;
	void remove(string cheie) override;
};