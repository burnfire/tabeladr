#pragma once
#include <string>
#include <memory>
#include <vector>
#include <sstream>
using namespace std;

class Serializabil
{
protected:
	/*serialize - serializeaza obiectul curent. Metoda pur virtuala*/
	virtual string serialize() const = 0;
};