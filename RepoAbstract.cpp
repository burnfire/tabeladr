#include "RepoAbstract.h"

void RepoSerializabil::add(string cheie, shared_ptr<Serializabil> obiect)
{
	if (exists(cheie))
		throw RepoError("Aplicatia contine deja substanta cu codul " + cheie + ".");
	all[cheie] = obiect;
}

void RepoSerializabil::modify(string cheie, shared_ptr<Serializabil> obiect)
{
	if (!exists(cheie))
		throw RepoError("Nu exista substanta cu codul ONU " + cheie + " in aplicatie.");
	all[cheie] = obiect;
}

void RepoSerializabil::remove(string cheie)
{
	if (!exists(cheie))
		throw RepoError("Nu exista substanta cu codul " + cheie + " in aplicatie.");
	all.erase(cheie);
}

template<class T>
bool RepoAbstract<T>::exists(string cheie) const
{
	return all.find(cheie) != all.end();
}

void RepoSubstanta::add(string cheie, Substanta obiect)
{
	/*if (exists(cheie))
		throw RepoError("Aplicatia contine deja substanta cu codul " + cheie + ".");
	all[cheie] = make_shared<Substanta>(obiect);*/
	RepoSerializabil::add(cheie, make_shared<Substanta>(obiect));
}

void RepoSubstanta::modify(string cheie, Substanta obiect)
{
	/*if (!exists(cheie))
		throw RepoError("Nu exista substanta cu codul ONU " + cheie + " in aplicatie.");
	all[cheie] = make_shared<Substanta>(obiect);*/
	RepoSerializabil::modify(cheie, make_shared<Substanta>(obiect));
}

void RepoSubstanta::remove(string cheie)
{
	/*if (!exists(cheie))
		throw RepoError("Nu exista substanta cu codul " + cheie + " in aplicatie.");
	all.erase(cheie);*/
	RepoSerializabil::remove(cheie);
}

Substanta RepoSubstanta::operator[](string cheie)
{
	if (!exists(cheie))
		throw RepoError("Aceasta substanta nu exista!");
	return *dynamic_pointer_cast<Substanta>(all[cheie]);
}

int RepoSubstanta::size() const
{
	return all.size();
}

vector<Substanta> RepoSubstanta::getAll()
{
	vector<Substanta> rez;
	transform(all.begin(), all.end(), back_inserter(rez), [&](const auto& p)
		{
			return this->operator[](p.first);
		});
	return rez;
}

void FileRepoSubstanta::load()
{
	ifstream f(path);
	while (f)
	{
		string linie;
		getline(f, linie);
		if (linie != "")
		{
			auto s = DeserializatorSubstanta::deserialize(linie);
			all.emplace(s.getCod(), make_shared<Substanta>(s));
		}
	}
}

void FileRepoSubstanta::save()
{
	ofstream g(path);
	for (const auto& p : all)
	{
		g << this->operator[](p.first).serialize() << endl;
	}
}

void FileRepoSubstanta::add(string cheie, Substanta obiect)
{
	RepoSubstanta::add(cheie, obiect);
	save();
}

void FileRepoSubstanta::modify(string cheie, Substanta obiect)
{
	RepoSubstanta::modify(cheie, obiect);
	save();
}

void FileRepoSubstanta::remove(string cheie)
{
	RepoSubstanta::remove(cheie);
	save();
}
