#include "ValidatorSubstanta.h"

bool ValidatorSubstanta::testCampuriGoale(Substanta s)
{
	return s.getDenumire().empty() || s.getCod().empty() || s.getClasa().empty() || s.getClasificare().empty();
}

bool ValidatorSubstanta::testClasaCorecta(Substanta s)
{
	for (const auto& c : claseDisponibile)
		if (s.getClasa() == c)
			return true;
	return false;
}

bool ValidatorSubstanta::testGrupaTransportCorecta(Substanta s)
{
	return s.getGrupaTransport()>=0&&s.getGrupaTransport()<=4;
}

bool ValidatorSubstanta::testSituatieCorecta(Substanta s)
{
	return s.situatie=="OK"||s.situatie=="INTERZIS"||s.situatie=="nonADR";
}

bool ValidatorSubstanta::testCantitateCorecta(Substanta s)
{
	return s.cantitLimit>=0;
}
